/***************************************************************************
sircd.c  - description
***************************************************************************/
/***************************************************************************
*                                                                         *
*   This simple irc server/daemon takes input from stdin as client        *
*   commands sent to the server. The server processes the input and       *
*   replies.                                                              *
*                                                                         *
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include "debug.h"
#include "rtlib.c"
//#include "rtgrading.h"
#include "sircd.h"
#include "irc_proto.c"

typedef struct sockaddr SA;

u_long curr_nodeID;
rt_config_file_t   curr_node_config_file;  /* The config_file  for this node */
rt_config_entry_t * curr_node_config_entry; /* The config_entry for this node */

void init_node(char * nodeID, char * config_file);
void irc_server();
void init_pool(int listenfd, pool * p);
void add_client(int connectfd, pool * client_pool);

void
usage() {
    fprintf(stderr, "sircd [-h] [-D debug_lvl] <nodeID> <config file>\n");
    exit(-1);
}


int main( int argc, char *argv[] ) {
    extern char *optarg;
    extern int optind;
    int opt = 1;
    int ch, listenfd , addrlen , connectfd , t, i , readlen , fd;
    struct sockaddr_in servaddr;
    pool client_pool;
    char buffer[1025];  //data buffer of 1K

    while ((ch = getopt(argc, argv, "hD:")) != -1)
    switch (ch) {
        case 'D':
        if (set_debug(optarg)) {
            exit(0);
        }
        break;
        case 'h':
        default: // FALLTHROUGH
        usage();
    }
    argc -= optind;
    argv += optind;

    if (argc < 2) {
        usage();
    }

    init_node(argv[0], argv[1]);

    printf( "I am node %lu and I listen on port %d for new users\n",
    curr_nodeID, curr_node_config_entry->irc_port );



    //a message
    char *message = "IRC Server\n\n";

    //initialise all client_socket[] to 0 so not checked
    for (i = 0; i < MAX_CLIENTS; i++)
    {
        client_pool.clients[i].sock = 0;
    }

    //create a master socket
    if( (listenfd = socket(AF_INET , SOCK_STREAM , 0)) == 0)
    {
        printf("socket failed");
        exit(1);
    }

    //set master socket to allow multiple connections , this is just a good habit, it will work without this
    if( setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )
    {
        printf("setsockopt");
        exit(1);
    }

    //type of socket created
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons( 6667 );

    //bind the socket to localhost port 8888
    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr))<0)
    {
        printf("bind failed");
        exit(1);
    }
    printf("Listener on port %d \n", 6667);

    //try to specify maximum of 3 pending connections for the master socket
    if (listen(listenfd, 3) < 0)
    {
        printf("listen");
        exit(1);
    }

    //accept the incoming connection
    addrlen = sizeof(servaddr);
    printf("Waiting...\n");

    while(1)
    {
        //clear the socket set
        FD_ZERO(&client_pool.read_set);

        //add master socket to set
        FD_SET(listenfd, &client_pool.read_set);
        client_pool.max_fd = listenfd;

        //add child sockets to set
        for ( i = 0 ; i < MAX_CLIENTS ; i++)
        {
            //socket descriptor
            fd = client_pool.clients[i].sock;

            //if valid socket descriptor then add to read list
            if(fd > 0)
                FD_SET( fd , &client_pool.read_set);

            //highest file descriptor number, need it for the select function
            if(fd > client_pool.max_fd)
                client_pool.max_fd = fd;
        }

        //wait for an activity on one of the sockets , timeout is NULL , so wait indefinitely
        t = select( client_pool.max_fd + 1 , &client_pool.read_set , NULL , NULL , NULL);

        if ((t < 0))
        {
            printf("select error");
        }

        //If something happened on the master socket , then its an incoming connection
        if (FD_ISSET(listenfd, &client_pool.read_set))
        {
            if ((connectfd = accept(listenfd, (struct sockaddr *)&servaddr, (socklen_t*)&addrlen))<0)
            {
                printf("accept");
                exit(1);
            }

            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , connectfd , inet_ntoa(servaddr.sin_addr) , ntohs(servaddr.sin_port));

            //send new connection greeting message
            if( send(connectfd, message, strlen(message), 0) != strlen(message) )
            {
                printf("send");
            }

            puts("Welcome message sent successfully");

            //add new socket to array of sockets
            for (i = 0; i < MAX_CLIENTS; i++)
            {
                //if position is empty
                if( client_pool.clients[i].sock == 0 )
                {
                    client_pool.clients[i].sock = connectfd;
                    printf("Adding to list of sockets as %d\n" , i);

                    break;
                }
            }
        }

        //else its some IO operation on some other socket :)
        for (i = 0; i < MAX_CLIENTS; i++)
        {
            fd = client_pool.clients[i].sock;

            if (FD_ISSET( fd , &client_pool.read_set))
            {
                //Check if it was for closing , and also read the incoming message
                if ((readlen = read( fd , buffer, 1024)) == 0)
                {
                    //Somebody disconnected , get his details and print
                    getpeername(fd , (struct sockaddr*)&servaddr , (socklen_t*)&addrlen);
                    printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(servaddr.sin_addr) , ntohs(servaddr.sin_port));

                    //Close the socket and mark as 0 in list for reuse
                    close( fd );
                    client_pool.clients[i].sock = 0;
                }

                //Handle the received line.
                else
                {
                    //set the string terminating NULL byte on the end of the data read
                    buffer[readlen] = '\0';
                    handle_line((char *)&buffer, fd, &client_pool);
                }
            }
        }
    }

    return 0;

}

/*
* Takes care of initializing a node for an IRC server
* from the given command line arguments
*/
void
init_node(char *nodeID, char *config_file)
{
    int i;

    curr_nodeID = atol(nodeID);
    rt_parse_config_file("sircd", &curr_node_config_file, config_file );

    /* Get config file for this node */
    for( i = 0; i < curr_node_config_file.size; ++i )
    if( curr_node_config_file.entries[i].nodeID == curr_nodeID )
    curr_node_config_entry = &curr_node_config_file.entries[i];

    /* Check to see if nodeID is valid */
    if( !curr_node_config_entry )
    {
        printf( "Invalid NodeID\n" );
        exit(1);
    }
}

/*
 * Initialize the pool p with listenfd
 */
void init_pool(int listenfd, pool *p)
{
    printf("Beginning init_pool\n");
    int i;
    p->max_i = -1;
    for ( i = 0; i < FD_SETSIZE; i++ )
        p->clients[i].sock = -1;

    /* Initially, listenfd is only member of select read set */
    p->max_fd = listenfd;
}

void add_client(int connectfd, pool * client_pool) {
    printf("\n");
    int i;
    client_pool->nready--;
    for ( i = 0; i < MAX_CLIENTS; i++ ) { /* Find an empty element */
        if ( client_pool->clients[i].sock < 0 ) {
            /* Add connected descriptor to the pool */
            client_pool->clients[i].sock = connectfd;
            //Rio_readinitb(&client_pool.clientrio[i], connectfd);

            /* Add the descriptor to descriptor set */
            FD_SET(connectfd, &client_pool->read_set);

            /* Update max descriptor and pool highwater mark */
            if (connectfd > client_pool->max_fd) {
                client_pool->max_fd = connectfd;
            }
            if (i > client_pool->num_clients) {
                client_pool->num_clients = i;
            }
            break;
        }
    }
    return;
}
