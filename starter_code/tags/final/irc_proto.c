#include "irc_proto.h"
#include "debug.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include "sircd.h"

#define MAX_COMMAND 16

/*
 * Defining CMD_ARGS to pass to all command functions.
 */

#define CMD_ARGS pool * client_pool, int connectfd, char *prefix, char **params, int n_params
typedef void (*cmd_handler_t)(CMD_ARGS);
#define COMMAND(cmd_name) void cmd_name(CMD_ARGS)


struct dispatch {
    char cmd[MAX_COMMAND];
    int needreg; /* Must the user be registered to issue this cmd? */
    int minparams; /* send NEEDMOREPARAMS if < this many params */
    cmd_handler_t handler;
};


#define NELMS(array) (sizeof(array) / sizeof(array[0]))

/* Define the command handlers here.  This is just a quick macro
* to make it easy to set things up */
COMMAND(cmd_nick);
COMMAND(cmd_user);
COMMAND(cmd_quit);
COMMAND(cmd_join);
COMMAND(cmd_part);
COMMAND(cmd_list);
COMMAND(cmd_privmsg);
COMMAND(cmd_who);

client_t * get_client_by_connectfd(int connectfd, pool * client_pool);
client_t * get_client_by_name(char * name, pool * client_pool);
int check_nick(char * nn, pool * client_pool);
int check_channel(char * cn);
int check_client_in_channel(int connectfd,int chanid);
void motd(client_t * client);


/* Dispatch table.  "reg" means "user must be registered in order
* to call this function".  "#param" is the # of parameters that
* the command requires.  It may take more optional parameters.
*/
struct dispatch cmds[] = {
    /* cmd,    reg #param function */
    { "NICK",    0, 0, cmd_nick },
    { "USER",    0, 4, cmd_user },
    { "QUIT",    1, 0, cmd_quit },
    { "JOIN",    1, 1, cmd_join },
    { "PART",    1, 1, cmd_part },
    { "LIST",    1, 0, cmd_list },
    { "PRIVMSG", 1, 0, cmd_privmsg },
    { "WHO",     1, 0, cmd_who }
};

/*
 * Handle a command line on connectfd.
 */
void
handle_line(char *line, int connectfd, pool * client_pool)
{
    char *prefix = NULL, *command, *pstart, *params[MAX_MSG_TOKENS];
    int n_params = 0;
    char *trailing = NULL;
    char temp[1024];
    client_t * client = get_client_by_connectfd(connectfd, client_pool);
    DPRINTF(DEBUG_INPUT, "Handling line: %s\n", line);
    command = line;
    if (*line == ':') {
        prefix = ++line;
        command = strchr(prefix, ' ');
    }
    if (!command || *command == '\0') {
        // Send ERR_UNKNOWNCOMMAND
        strcpy(temp, command);
        sprintf(temp + strlen(command), " :Unknown command\n");
        write(connectfd, temp, strlen(temp));
        return;
    }

    while (*command == ' ') {
        *command++ = 0;
    }
    if (*command == '\0') {
        // Send ERR_UNKNOWNCOMMAND
        strcpy(temp, command);
        sprintf(temp + strlen(command), " :Unknown command\n");
        write(connectfd, temp, strlen(temp));
        return;
    }
    pstart = strchr(command, ' ');
    if (pstart) {
        while (*pstart == ' ') {
            *pstart++ = '\0';
        }
        if (*pstart == ':') {
            trailing = pstart;
        } else {
            trailing = strstr(pstart, " :");
        }
        if (trailing) {
            while (*trailing == ' ')
            *trailing++ = 0;
            if (*trailing == ':')
            *trailing++ = 0;
        }

        do {
            if (*pstart != '\0') {
                params[n_params++] = pstart;
            } else {
                break;
            }
            pstart = strchr(pstart, ' ');
            if (pstart) {
                while (*pstart == ' ') {
                    *pstart++ = '\0';
                }
            }
        } while (pstart != NULL && n_params < MAX_MSG_TOKENS);
    }

    if (trailing && n_params < MAX_MSG_TOKENS) {
        params[n_params++] = trailing;
    }

    DPRINTF(DEBUG_INPUT, "Prefix:  %s\nCommand: %s\nParams (%d):\n",
    prefix ? prefix : "<none>", command, n_params);
    int i;
    for ( i = 0; i < n_params; i++ ) {
        DPRINTF(DEBUG_INPUT, "   %s\n", params[i]);
    }
    DPRINTF(DEBUG_INPUT, "\n");

    for ( i = 0; i < NELMS(cmds); i++ ) {
        if (!strcasecmp(cmds[i].cmd, command)) {
            if ( cmds[i].needreg && get_client_by_connectfd(connectfd, client_pool)->registered == 0 ) {
                sprintf(temp, ":You have not registered\n");
                write(connectfd, temp, strlen(temp));
                return;
                // ERROR - the client is not registered and they need
                // to be in order to use this command!
            } else if (n_params < cmds[i].minparams) {
                // ERROR - the client didn't specify enough parameters
                // for this command!
                strcpy(temp, command);
                sprintf(temp + strlen(command), " :Not enough parameters\n");
                write(connectfd, temp, strlen(temp));
                return;
            } else {
                // handle the command, and register the client if possible.
                (*cmds[i].handler)(client_pool, connectfd, prefix, params, n_params);
                if ( client->has_nick &&
                client->has_user &&
                client->registered == 0 ) {
                    client->registered = 1;
                }
            }
            break;
        }
    }
    if (i == NELMS(cmds)) {
        // Send ERR_UNKNOWNCOMMAND
        strcpy(temp, command);
        sprintf(temp + strlen(command), " :Unknown command\n");
        write(connectfd, temp, strlen(temp));
        return;
    }
}

/*
 * Begin command handlers
 * These are executed whenever a client sends a valid command.
 * Each creates a buffer and writes to that before sending.
 */
void cmd_nick(CMD_ARGS)
{
    printf("%d: NICK\n", connectfd);
    char nn[MAX_MSG_LEN+1];
    sprintf(nn, params[0], strlen(params[0]));
    char buf[1024];
    client_t * client = get_client_by_connectfd(connectfd, client_pool);
    if ( check_nick(nn, client_pool) == 0 ) {
        client->has_nick = 1;
        strcpy(client->nick, nn);
    } else {
        // Send ERR_NICKNAMEINUSE
        strcpy(buf, nn);
        sprintf(buf + strlen(nn), " :Nickname is already in use\n");
        write(connectfd, buf, strlen(buf));
    }
}

void cmd_user(CMD_ARGS)
{
    printf("%d: USER\n", connectfd);
    char un[MAX_USERNAME];
    sprintf(un, params[0], strlen(params[0]));
    char hn[MAX_HOSTNAME];
    sprintf(hn, params[1], strlen(params[1]));
    char sv[MAX_SERVERNAME];
    sprintf(sv, params[2], strlen(params[2]));
    char rn[MAX_REALNAME];
    sprintf(rn, params[3], strlen(params[3]));
    char buf[1024];
    client_t * client = get_client_by_connectfd(connectfd, client_pool);

    if ( client->has_user == 1 ) {
        // Send ERR_ALREADYREGISTRED
        sprintf(buf, ":You may not register\n");
        write(connectfd, buf, strlen(buf));
    } else {
        strcpy(client->user, un);
        strcpy(client->hostname, hn);
        strcpy(client->servername, sv);
        strcpy(client->realname, rn);
        client->has_user = 1;
        if ( client->has_nick == 1 ) {
            client->registered = 1;
        }
        // If the client has been registed, call motd
        if ( (client->has_nick == 1) && (client->has_user == 1) ) {
            motd(client);
        }
        sprintf(buf, "Set user:%s hostname:%s realname:%s\n",un,hn,rn);
        write(connectfd, buf, strlen(buf));
    }
}

void cmd_quit(CMD_ARGS)
{
    printf("%d: QUIT\n", connectfd);
    client_t * client = get_client_by_connectfd(connectfd, client_pool);
    close(connectfd);
    client->sock = -1;
}

void cmd_join(CMD_ARGS)
{
    printf("%d: JOIN\n", connectfd);
    int id, i, s;
    char * cn = params[0];
    channel_t * channel;
    printf("Getting client\n");
    client_t * client = get_client_by_connectfd(connectfd, client_pool);
    if ( (id = check_channel(cn)) == -1 ) {
        //create a new channel
        printf("Making a new channel\n");
        channel = (channel_t *) malloc(sizeof(channel_t));
        s = 1;
        strcpy(channel->channame, cn);
        channel->is_on = 1;
        channel->client_count = 1;
        // initialize the channel
        for ( i = 0; i < MAX_CLIENTS; i++ ) {
            channel->connected_clients[i] = 0;
        }
        channel->connected_clients[channel->client_count] = connectfd;
        // allocate this new channel to the channel list
        printf("Allocating the new channel a space in the list.\n");
        for ( i = 0; i < MAX_CHANNELS; i++ ) {
            if ( channel_list[i] == NULL ){
                channel_list[i] = channel;
                channel->chanid = i;
                free(channel);
                break;
            }
        }
        channel_count++;
    } else {
        // Add the client to a existed channel
        printf("Adding client to existing channel\n");
        channel = channel_list[id];
        channel->client_count++;
        channel->connected_clients[channel->client_count] = connectfd;
    }
    char buf[1024];
	sprintf(buf, ":%s JOIN %s\n", client->nick, cn);

    printf("Getting hostname\n");
	char hostname[1024];
	gethostname(hostname, sizeof(hostname));

    printf("Printing list of NAMES\n");
	sprintf(buf,"%s:%s 353 %s = %s : %s\n", buf, hostname, client->nick, cn, client->nick);
	sprintf(buf,"%s:%s 366 %s %s :End of /NAMES list\n", buf, hostname, client->nick, cn);
	for ( i = 0; i < MAX_CLIENTS; i++ ) {
		if ( channel->connected_clients[i] != 0 ) {
			write(channel->connected_clients[i], buf, strlen(buf));
		}
	}
    printf("Freeing if necessary\n");
    if ( s ) {
        free(channel);
    }
}

void cmd_part(CMD_ARGS)
{
    printf("%d: PART\n", connectfd);
    int id;
    char buf[1024];
    char * cn = params[0];
    channel_t * channel;
    DPRINTF(2, "Beginning PART\n");
    client_t * client = get_client_by_connectfd(connectfd, client_pool);
    if ( (id = check_channel(cn)) != -1 ) {
        if ( check_client_in_channel(connectfd, id) == 1 ) {
            channel = channel_list[id];
            channel->client_count--;
            channel->connected_clients[channel->client_count] = 0;
            if ( channel->client_count == 0 ) {
                //there is no clients in this channel,so delete it
                channel_list[id] = NULL;
            } else {
                //broadcast the quit message to all the other clients in this channel
                sprintf(buf,":%s!++++++++@%s QUIT :\n",client->nick,channel->channame);
                write(connectfd, buf, strlen(buf));
                int i;
                for ( i = 0; i < MAX_CLIENTS; i++ ) {
                    if ( channel->connected_clients[i] != 0 ) {
                        write(channel->connected_clients[i], buf, strlen(buf));
                    }
                }
            }
        } else {
            strcpy(buf, cn);
            sprintf(buf + strlen(cn), " :You are not on that channel\n");
            write(connectfd, buf, strlen(buf));
        }
    } else {
        strcpy(buf, cn);
        sprintf(buf + strlen(cn), " :No such channel\n");
        write(connectfd, buf, strlen(buf));
    }
}

void cmd_who(CMD_ARGS)
{
    printf("%d: WHO\n", connectfd);
    DPRINTF(2, "Beginning WHO\n");
    char * cn = params[0];
    int id = check_channel(cn);
    char buf[1024];
    char hostname[1024];
	gethostname(hostname, sizeof(hostname));

    if (id != -1) {
        client_t * client = get_client_by_connectfd(connectfd, client_pool);
        channel_t * channel = channel_list[id];
        sprintf(buf, ":%s 352 %s %s please look out: ", hostname, client->nick, channel->channame);
		int i;
		client_t *temp = NULL;
		for ( i = 0; i < MAX_CLIENTS; i++ ) {
			if ( channel->connected_clients[i] != 0 ) {
				temp = get_client_by_connectfd(i, client_pool);
				sprintf(buf,"%s %s",buf,temp->nick);
			}
		}
		sprintf(buf,"%s H :0 The MOTD\n",buf);
		sprintf(buf,"%s:%s 315 %s %s :End of /WHO list\n", buf, hostname, client->nick, channel->channame);
		write(connectfd, buf, strlen(buf));

    } else {
        strcpy(buf, cn);
        sprintf(buf + strlen(cn)," :No such channel\n");
        write(connectfd,buf,strlen(buf));
    }
}

void cmd_list(CMD_ARGS)
{
    printf("%d: LIST\n", connectfd);
    char hostname[1024];
	gethostname(hostname, sizeof(hostname));
	client_t * client = get_client_by_connectfd(connectfd, client_pool);
	char buf[1024];
	sprintf(buf,":%s 321 %s Channel :Users Name\n",hostname, client->nick);

	int i;
	for ( i = 0; i < MAX_CHANNELS; i++ ) {
		if ( channel_list[i] != NULL ) {
			sprintf(buf,"%s:%s 322 %s %s :%d\n", buf, hostname, client->nick, channel_list[i]->channame, channel_list[i]->client_count);

		}
	}
	sprintf(buf,"%s:%s 323 %s :End of /LIST\n", buf, hostname, client->nick);
	write(connectfd, buf, strlen(buf));
}

void cmd_privmsg(CMD_ARGS)
{
    printf("%d: PRIVMSG\n", connectfd);
    int id;
    char target[1024];
    sprintf(target, params[0], strlen(params[0]));
    char m[MAX_MSG_LEN];
    sprintf(m, params[1], strlen(params[1]));
    char buf[1024];
    if ( check_nick(target, client_pool) == 1 ) {
        // send message to a client
        DPRINTF("send message to a client\n");
        client_t * target_client = get_client_by_name(target, client_pool);
        client_t * current_client = get_client_by_connectfd(connectfd, client_pool);
        sprintf(buf,":%s PRIVMSG %s :%s\n", current_client->nick, target_client->nick, m);
        write(target_client->sock, buf, strlen(buf));
    }
    else if ( (id = check_channel(target)) != -1 ) {
        // send message to a channel
        DPRINTF("send message to a channel\n");
        int i;
        channel_t *target_channel = channel_list[id];
        for ( i = 0; i < MAX_CLIENTS; i++ ) {
            //boardcast the message to all the clients in the channel
            if ( (target_channel->connected_clients[i] != 0) && (target_channel->connected_clients[i] != connectfd) ) {
                sprintf(buf, ":%s PRIVMSG %s :%s\n", get_client_by_connectfd(connectfd, client_pool)->nick, target_channel->channame, m);
                write(target_channel->connected_clients[i], buf, strlen(buf));
            }
        }

    } else {
        sprintf(buf,":No recipient given (PRIVMSG)\n");
        write(connectfd, buf, strlen(buf));
    }
}


/* End command handlers */

/*
 * Get client in the client list by connectfd
 */
client_t * get_client_by_connectfd(int connectfd, pool * client_pool)
{
    int i;
    for ( i = 0; i < MAX_CLIENTS; i++ ) {
        printf("i: %d\n", i);
        if ( client_pool->clients[i].sock == connectfd ) {
            return &client_pool->clients[i];
        }
    }
    return NULL;
}
/*
 * Get client in the client list by connectfd
 */
client_t * get_client_by_name(char * name, pool * client_pool)
{
    int i;
    for ( i = 0; i < MAX_CLIENTS; i++ ) {
		if ( &client_pool->clients[i] != NULL ) {
			if ( strcmp(client_pool->clients[i].nick, name) == 0 ) {
                return &client_pool->clients[i];
            }
		}
	}
    return NULL;
}
/*
 * Check if the nickname already exists
 */
int check_nick(char * nn, pool * client_pool)
{
    int i;
	for ( i = 0; i < MAX_CLIENTS; i++ ) {
		if ( &client_pool->clients[i] != NULL ) {
			if( strcmp(client_pool->clients[i].nick, nn) == 0 ) {
                return 1;
            }
		}
	}
  	return 0;
}
/*
 * check whether the channel exists
 */
 int check_channel(char * cn)
 {
    int i;
 	for ( i = 0; i < MAX_CHANNELS; i++ ){
 		if ( channel_list[i] != NULL ) {
 			if ( strcmp(channel_list[i]->channame, cn) == 0 ) {
                 return channel_list[i]->chanid;
             }
 		}
 	}
   	return -1;
 }
/*
 * Check if the client is in the channel
 */
int check_client_in_channel(int connectfd,int chanid)
{
    channel_t *channel = channel_list[chanid];
    int i;
    for ( i = 0; i < MAX_CLIENTS; i++ ) {
        if ( (channel->connected_clients[i]) == connectfd ) {
            return 1;
        }
    }
    return -1;
}
/*
 * Return the Message of the Day when a client has been registed.
 */
void motd(client_t * client)
{
    printf("%d: MOTD\n", client->sock);
    char hostname[1024];
    char buf[MAX_MSG_LEN];
    gethostname(hostname, sizeof(hostname));
    sprintf(buf,":%s 375 %s :Message of the day: 'Hi!'\n", hostname, client->nick);
	write(client->sock, buf, strlen(buf));
}
