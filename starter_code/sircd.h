#ifndef _SIRCD_H_
#define _SIRCD_H_

#include <sys/types.h>
#include <netinet/in.h>

#define MAX_CLIENTS FD_SETSIZE
#define MAX_MSG_TOKENS 10
#define MAX_MSG_LEN 512
#define MAX_USERNAME 32
#define MAX_HOSTNAME 512
#define MAX_SERVERNAME 512
#define MAX_REALNAME 512
#define MAX_CHANNAME 512
#define MAX_CHANNELS 64

typedef struct client_struct {
    int sock;
    struct sockaddr_in cliaddr;
    unsigned inbuf_size;
    int registered;
    char hostname[MAX_HOSTNAME];
    char servername[MAX_SERVERNAME];
    char user[MAX_USERNAME];
    char nick[MAX_USERNAME];
    char realname[MAX_REALNAME];
    char inbuf[MAX_MSG_LEN+1];
    char channel[MAX_CHANNAME];
    int has_nick;
    int has_user;
} client_t;

typedef struct pool_struct {
    int num_clients;
    int max_fd;
    int max_i;
    int nready;
    fd_set read_set;
    fd_set ready_set;
    fd_set write_set;
    client_t clients[MAX_CLIENTS];
} pool;

typedef struct channel_struct {
    int chanid;
    char channame[16];
    int connected_clients[MAX_CLIENTS];
	int is_on;
	int client_count;
} channel_t;


/* Global variables */
u_long curr_nodeID;
rt_config_file_t   curr_node_config_file;  /* The config_file  for this node */
rt_config_entry_t *curr_node_config_entry; /* The config_entry for this node */
channel_t *channel_list[10]; /* The channel list */
int client_count; /* The client count */
int channel_count = 0; /* The client count */

#endif /* _SIRCD_H_ */
